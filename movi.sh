#!/bin/sh
WATCH_REPLACE_ESC=$(echo "${WATCH_REPLACE}" | sed 's/[*/.&]/\\&/g')
echo "Start watching $WATCH_REPLACE with inotifywait..."
inotifywait -r -e moved_to --monitor "/watch" --exclude "@eaDir" | \
  while read -r notify;
  do
    notify=$(echo "${notify}" | sed "s/\/watch/${WATCH_REPLACE_ESC}/" | sed "s/MOVED_TO,ISDIR/MOVED_TO/" | sed "s/'/''/")
    folder="${notify%%/ MOVED_TO *}"
    file="${notify#${folder}/ MOVED_TO }"
    echo "'${file}' moved to '${folder}'"
    eval "psql -h 127.0.0.1 -U postgres download -c \"UPDATE download_queue SET destination = '${folder}' WHERE filename = '${file}'\""
  done
