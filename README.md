# movish

Movish is a small Docker image helping to manage files shared with Download Station on Synology NAS.

Movish watches recursively a folder for file or directory movements into this folder and updates related download task destination in Download Station through its DB.

Several files and/or directories can be moved at the same time but with limitation that moving a parent directory will not update destination of children files.

## Installation

2 prerequisites:

### Docker

Having Docker installed either with official package or by manual installation in Package manager (compatible with Intel 64bits processors): https://archive.synology.com/download/Package/Docker

### Open PostgreSQL

Download Station uses PostgreSQL as DB, you need to open loopback connection to allow Docker container to request PostgreSQL:

Create a new User-defined script Trigered Task in Control Panel > Task Scheduler:
- Task name: Open PostgreSQL
- User : root
- Event : boot-up
- Task Settings > User-defined script:
```
sudo cp /etc/postgresql/pg_hba.conf /etc/postgresql/user.conf.d/pg_hba.conf
sudo chown postgres:postgres /etc/postgresql/user.conf.d/pg_hba.conf
echo "host    all             all             127.0.0.1/32            trust" | sudo tee -a /etc/postgresql/user.conf.d/pg_hba.conf
echo "hba_file = '/etc/postgresql/user.conf.d/pg_hba.conf'" | sudo tee /etc/postgresql/user.conf.d/postgresql.user.conf
```

You can activate logs and run the script.

### Installation of movish

Several options are possible to run a Docker container, either:

- Use SSH:

Connect in SSH to Synology and run this command: `sudo docker run -dit --pull always --name movish --network host -v /volume1/data/video:/watch:ro -e WATCH_REPLACE=data/video --restart unless-stopped arnor2000/movish-synology:latest`
Where you adapt volume and `WATCH_REPLACE` environment variable to your need.
A Docker container know nothing outside its container, `WATCH_REPLACE` is required to indicate where files it watches on host filesystem are really.

- Use Synology Docker GUI:

Open Docker application in DiskStation, Create a container, Download an image, Search for `movish-synology` by user `arnor2000`.
On Next page, select Use the same network as Docker Host.
Next, Enable auto-restart.
In advanced Settings, Add `WATCH_REPLACE` Environment Variable with value, the path to the folder you want movish to watch (same as next Volume Settings).
Next, in Volume Settings add the folder you want to watch and mount it to path `/watch` in Read-Only.

- Follow this guide to setup a CI/CD from Git to Docker on Synology : https://keestalkstech.com/2019/11/docker-on-synology-from-git-to-running-container-the-easy-way/

## Usage

Move or cut/paste files and directories shared with Download Station on Synology NAS by any way you want.

Several files and/or directories can be moved at the same time but with limitation that moving a parent directory will not update destination of children files.
