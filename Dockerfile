FROM alpine:latest
RUN apk --update add inotify-tools postgresql-client
WORKDIR /home
COPY movi.sh movi.sh
RUN addgroup -S movigroup
RUN adduser -S moviuser -G movigroup
RUN chown moviuser:movigroup movi.sh
RUN chmod +x movi.sh
USER moviuser
CMD ["./movi.sh"]